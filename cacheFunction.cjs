function cacheFunction(cb) {
  const cache = {};

  try {
    return function (parameter) {
      //closure cache used here if the cache has not the parameter then it will store the parameter and the value returned by cb as key value pair
      if (!cache[parameter]) {
        let cbResult = cb(parameter);
        cache[parameter] = cbResult;
        return cbResult;
      } else {
        return cache[parameter];
      }
    };
  } catch (error) {
    console.log(error);
    return;
  }
}

module.exports = cacheFunction;
