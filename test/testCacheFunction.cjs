const cacheFunction = require("../cacheFunction.cjs");

//the cb is the callBack function passed in cacheFunction
function cb(parameter) {
  return "hello " + parameter;
}

let returnedFunction = cacheFunction(cb);

console.log(returnedFunction("john"));
console.log(returnedFunction("daniel"));
console.log(returnedFunction("remo"));
console.log(returnedFunction("john"));
