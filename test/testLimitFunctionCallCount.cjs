const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

//cb is the callback function passed to limitFunctionCallCount
function cb() {
  console.log("callback is called");
}

let returnedFunction = limitFunctionCallCount(cb, 2);
returnedFunction();
returnedFunction();
returnedFunction();
returnedFunction();
