function limitFunctionCallCount(cb, n) {
  try {
    return function () {
      //checking if cb is invoked less than n times or not
      if (n > 0) {
        cb();
        n--;
      } else {
        return null;
      }
    };
  } catch (error) {
    console.log(error);
    return;
  }
}


module.exports = limitFunctionCallCount;
