function counterFactory() {
  let counter = 0;
  //try to return object with increment and decrement methods
  try {
    return {
      increment: function () {
        counter += 1;
        return counter;
      },
      decrement: function () {
        counter -= 1;
        return counter;
      },
    };
  } catch (error) {
    return; //if any error occur then returning null
  }
}

module.exports = counterFactory;
